/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * randomcreature is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'randomcreature.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property string currentHead: "0" + getRandomInt(1,4).toString()
    property string currentBody: "0" + getRandomInt(1,4).toString()
    property string currentLegs: "0" + getRandomInt(1,4).toString()

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Random Creature')
        }

        Item {
            id: mainImage
            width: height * .6
            height: root.height - header.height
                //: root.width - header.height

            anchors.centerIn: parent

            Image {
                sourceSize.width: parent.width
                fillMode: Image.PreserveAspectFit
                source: Qt.resolvedUrl("../assets/creatures-areas/fig-%1-legs.svg".arg(currentLegs))
            }

            Image {
                sourceSize.width: parent.width
                fillMode: Image.PreserveAspectFit
                source: Qt.resolvedUrl("../assets/creatures-areas/fig-%1-body.svg".arg(currentBody))
            }

            Image {
                sourceSize.width: parent.width
                source: Qt.resolvedUrl("../assets/creatures-areas/fig-%1-head.svg".arg(currentHead))
                fillMode: Image.PreserveAspectFit
            }
        }

        MouseArea {
            anchors.fill: parent

            onClicked: getImages()
        }
    }

    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    function getImages() {
        var head = getRandomInt(1,13);
        var body = getRandomInt(1,11);
        head > 9
            ? currentHead = head.toString()
            : currentHead = "0" + head.toString();
        body > 9
            ? currentBody = body.toString()
            : currentBody = "0" + body.toString();
        currentLegs = "0" + getRandomInt(1,10).toString();
    }
}
